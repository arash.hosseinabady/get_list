<?php

namespace app\modules\get_list\controllers;

use yii\data\ActiveDataProvider;
use yii\helpers\Inflector;
use yii\web\Controller;
use yii\web\Response;

/**
 * Api controller for the `get_list` module
 */
class ApiController extends Controller
{
    /**
     * set response request in jason.
     *
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return parent::beforeAction($action);
    }

    /**
     * Provide the array of data in json.
     *
     * @return string[]
     */
    public function actionIndex()
    {
        $input = \Yii::$app->request->queryParams;

        if (!isset($input['source'])) {
            return [
                'status' => -1,
                'message' => 'Please set \'source\' parameter!'
            ];
        }

        $modelClass = 'app\models\\' . Inflector::camelize($input['source']);
        if (!class_exists($modelClass)) {
            return [
                'status' => -1,
                'message' => '\'source\' parameter is incorrect!'
            ];
        }
        $modelObj = new $modelClass();
        $columns = $modelObj->getTableSchema()->columns;

        $query = $modelClass::find();

        $pageSize = 20;
        if (isset($input['per_page'])) {
            $pageSize = $input['per_page'];
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $pageSize
            ]
        ]);

        if (isset($input['select'])) {
            $query->select($input['select']);
        }

        if (isset($input['search'])) {
            foreach ($input['search'] as $key => $value) {
                if ($value && isset($columns[$key])) {
                    if (in_array($columns[$key]->type, ['string', 'text'])) {
                        $query->andWhere(['like', $key, $value]);
                    } else {
                        $query->andWhere([$key => $value]);
                    }
                }
            }
        }

        try {
            $dataProvider->prepare();
        } catch (\Exception $e) {
            return [
                'status' => -1,
                'message' => $e->getMessage()
            ];
        }

        return [
            'status' => 0,
            'pagination' => [
                'totalCount' => $dataProvider->totalCount,
                'page' => $dataProvider->pagination->page + 1,
                'page_count' => (int) (($dataProvider->totalCount + $pageSize - 1) / $pageSize),
            ],
            'data' => $dataProvider->models,
        ];
    }
}
