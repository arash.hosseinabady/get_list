# get_list

## Getting started

To make it easy for you to get started with this, here's a list of recommended next steps.
This module developed for Yii2 framework.

## Requirements
- framework `Yii2`
- `php 7.4`
- `mysql 8.0`


## Installation

Download this module and add to your project in modules directory. To access the module, you need to add this to your application configuration:

```
'modules' => [
    'get-list2' => [
        'class' => 'app\modules\GetList2\Module',
    ],
],
```

## Usage
Call api controller and set parameters in querystring.

```
get-list/api?source={source_name}&
     select={list of field. separate with comma}&
     search[field_name1]={value}&search[field_name2]={value}&
     per_page={per_page_value}&
     page={number_of_page}
```

field `source` must be set in request. The `source` is model class in app.

return data in json.
if response is ok, then return data based on below structure:
```
 [
      status: int (value is 0),
      pagination: [
          totalCount: int (total count of data),
          page: int (number of current page),
          page_count: int (number of total page),
      ],
      data: [
          list of data based on request
      ]
 ]
```

if response has error, then return below data:
```
 [
      status: int (value is -1),
      message: "error message"
 ]
```
